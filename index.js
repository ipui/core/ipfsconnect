import React, { useState } from 'react'
import multiaddr from 'multiaddr'

import RouteTools from '../lib/RouteTools'

import {
  FiCheck
} from 'react-icons/fi'

function IpfsConnect( props ) {
  const { addresses } = RouteTools.parse()
  const [ address, setAddress ] = useState( '' )

  function updateAddress( event ) {
    setAddress( event.target.value )
  }

  function isAddressValid() {
    try {
      const {
        family,
        host,
        transport,
        port
      } = multiaddr( address ).toOptions()

      if ( family && host && transport && port )
        return true

      return false
    } catch ( e ) {
      // TODO implement error handler as toast
      console.error( e )
      return false
    }
  }

  function connect() {
    if ( isAddressValid() )
      return RouteTools._go( RouteTools.applyHash( address ), { reload: true } )
  }

  function onEnter( event ) {
    if ( event.keyCode === 13 )
      connect()
  }

  return (
    <>
      <input
        placeholder={ addresses.api }
        onChange={ updateAddress }
        onKeyDown={ onEnter }
        size="1"
      />

      <button onClick={ connect }>
        <FiCheck />
      </button>
    </>
  )

}

export default IpfsConnect
